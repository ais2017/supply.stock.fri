package repository;

import java.util.Collection;

/**
 * Created by admin on 03.12.2017.
 */
@org.springframework.stereotype.Repository
public interface Repository<T> {

    public Collection<T> getAll ();

}
