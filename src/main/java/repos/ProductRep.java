package repos;

import com.ais.main.Position;
import com.ais.main.Product;
import com.ais.main.Scenarios;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Vector;
import java.util.stream.Collectors;

public class ProductRep{
    public ProductRep(){}

    private static ArrayList<Product> products = new ArrayList<Product>();

    public static void save(Product product){
        Product productSearch = products.stream().filter(iter -> iter.getArticul().equals(product.getArticul())).findFirst().orElse(null);
        if (productSearch == null)
            products.add(product);
        else
        {
            products.forEach(iter -> {
                    if (iter.getArticul() == product.getArticul())
                        iter.resuplyProduct(product.getQuantity());
            });
        }
    }

    public static Collection<Product> getAll(){
        return products;
    }

    public static Product getForArticul(Integer articul){
        return products.stream().filter(iter ->
                iter.getArticul().equals(articul)).findFirst().orElse(null);
    }

    public static ArrayList<Product> generateProducts(){
        products.add(new Product(1, "Шпатлевка 50кг", "Мешок шпатлевки 50кг", 5, "Склад №1", "A1"));
        products.add(new Product(2, "Цемент 50кг", "Мешок цемента 50кг", 5, "Склад №1", "A1"));
        products.add(new Product(3, "Лента 10м", "Лента самоклейкая 10м", 5, "Склад №1", "A2"));
        products.add(new Product(4, "Молоко Домик в деревне 1л", "Молоко Домик в деревне 1л непастеризованное", 5, "Склад №2", "A1"));
        return products;
    }
    public static void clear(){
        products.clear();
    }
}
