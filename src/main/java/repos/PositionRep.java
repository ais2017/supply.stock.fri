package repos;

import com.ais.main.Position;
import com.ais.main.Product;
import com.ais.main.Scenarios;
import com.ais.main.Warehouse;

import java.lang.reflect.Array;
import java.util.*;
import java.util.stream.Collectors;

public class PositionRep{

    public PositionRep(){}

    private static ArrayList<Position> positions = new ArrayList<Position>();

    public static void save(Position position) {
        if(positions.stream().filter(iter -> iter.getPosition().equals(position.getPosition())).findFirst().orElse(null) == null) {
            positions.add(position);
        }
        else throw new IllegalArgumentException("ERROR: Позиция " + position.getPosition() + " уже существует! \n");
    }

    public static Collection<Position> getAll(){
        return positions;
    }

    public static Position getForPosition(String warehouseName, String pos){

        List<Position> positionsWH = positions.stream().filter(iter ->
                iter.getWarehouseName().equals(warehouseName)).collect(Collectors.toList());

        Position position = positionsWH.stream().filter(iter ->
                iter.getPosition().equals(pos)).findFirst().orElse(null);

        return position;
    }

    public static ArrayList<Position> generatePosition(){

        positions.add(new Position("Склад №1","A1", "Молочные продукты", new ArrayList<Product>()));
        positions.add(new Position("Склад №1","A2", "Промышленные материалы", new ArrayList<Product>()));
        positions.add(new Position("Склад №2","A1", "Молочные продукты" , new ArrayList<Product>()));
        positions.add(new Position("Склад №2","A2", "Продовольственная продукция", new ArrayList<Product>()));
        return positions;
    }

    public static void clear(){
        positions.clear();
    }

}
