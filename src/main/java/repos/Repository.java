package repos;

import java.util.Collection;

/**
 * Created by admin on 15.12.2017.
 */
public interface Repository<T> {

    public Collection<T> getAll();

    public void save(T t) throws Exception;

    public void clear();

}
