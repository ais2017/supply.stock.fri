package repos;

import com.ais.main.Position;
import com.ais.main.Warehouse;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class WarehouseRep {
    public WarehouseRep(){}
    private static ArrayList<Warehouse> warehouses = new ArrayList<Warehouse>();

    public static void save(Warehouse warehouse){
        warehouses.add(warehouse);
    }

    public static Collection<Warehouse> getAll(){
        return warehouses;
    }

    public static Warehouse getForName(String name){
        return warehouses.stream().filter(iter -> iter.getName().equals(name)).findFirst().orElse(null);
    }

    public static ArrayList<Warehouse> generateWarehouse(){
        warehouses.add(new Warehouse("Склад №1", "Каширское д. 44", new ArrayList<Position>()));
        warehouses.add(new Warehouse("Склад №2", "Каширское д. 45", new ArrayList<Position>()));
        warehouses.add(new Warehouse("Склад №3", "Каширское д. 46" , new ArrayList<Position>()));
        warehouses.add(new Warehouse("Склад №4", "Каширское д. 47", new ArrayList<Position>()));
        return warehouses;
    }

    public static void clear(){
        warehouses.clear();
    }
}