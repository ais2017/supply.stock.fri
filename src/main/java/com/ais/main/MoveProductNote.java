package com.ais.main;

import java.util.Date;
import java.util.Map;

/**
 * Created by admin on 24.11.2017.
 */
public class MoveProductNote {
    private Map<Product, Integer> products;
    private Date date;
    private String workerName;
    private String warehouseProvider;
    private String warehouseConsumer;
    private String status;

    public MoveProductNote(Map<Product, Integer> products, Date date, String workerName, String warehouseProvider, String warehouseConsumer, String status) {
        this.products = products;
        this.date = date;
        this.workerName = workerName;
        this.warehouseProvider = warehouseProvider;
        this.warehouseConsumer = warehouseConsumer;
        this.status = status;
    }

    public Integer addProduct(Product product, Integer quantity) {
        return this.products.put(product, quantity);
    }

    public Integer delProduct(Product product) {
        return this.products.remove(product);
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getWorkerName() {
        return workerName;
    }

    public void setWorkerName(String workerName) {
        this.workerName = workerName;
    }

    public String getWarehouseProvider() {
        return warehouseProvider;
    }

    public void setWarehouseProvider(String warehouseProvider) {
        this.warehouseProvider = warehouseProvider;
    }

    public String getWarehouseConsumer() {
        return warehouseConsumer;
    }

    public void setWarehouseConsumer(String warehouseConsumer) {
        this.warehouseConsumer = warehouseConsumer;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Map<Product, Integer> getProducts() {
        return products;
    }
}

