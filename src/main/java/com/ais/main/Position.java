package com.ais.main;

import com.ais.main.Product;

import java.util.Collection;

/**
 * Created by admin on 23.11.2017.
 */
public class Position {
    private String warehouseName;
    private String position;
    private String description;
    private Collection<Product> products;

    public Position(String warehouseName, String position, String description, Collection<Product> products) {
        this.warehouseName = warehouseName;
        this.position = position;
        this.description = description;
        this.products = products;
     }

    public Collection<Product> getProducts() {
        return products;
    }

    public String getWarehouseName() {
        return warehouseName;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean addProduct(Product product) {

        Product searchProduct = products.stream().filter(iter ->
                iter.getArticul().equals(product.getArticul())).findFirst().orElse(null);
        if(searchProduct == null)
            return this.products.add(product);  ///валится!
        else
        {
            searchProduct.resuplyProduct(product.getQuantity());
            return true;
        }
    }

    public boolean delProduct(Product product) {
        return this.products.remove(product);
    }
}
