package com.ais.main;

/**
 * Created by admin on 23.11.2017.
 */
public class Product {
    private Integer articul;
    private String name;
    private String description;
    private Integer quantity;
    private String warehouseName;
    private String position;

    public Product(Integer articul, String name, String description, Integer quantity, String warehouseName, String position) {
        this.articul = articul;
        this.name = name;
        this.description = description;
        this.quantity = quantity;
        this.warehouseName = warehouseName;
        this.position = position;
    }

    public Integer getArticul() {
        return articul;
    }

    public void setArticul(Integer articul) {
        this.articul = articul;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public boolean writeOffProduct(Integer qnt){
        if (this.quantity - qnt >= 0)
        {
            this.quantity -= qnt;
            return true;
        }
        else
        {
            System.out.println("На складе недостаточно товара, текущее кол-во имеющегося товара: " + this.quantity);//throw
            return false;
        }
        //throw
    }

    public void resuplyProduct(Integer qnt){
        this.quantity += qnt;
    }

}
