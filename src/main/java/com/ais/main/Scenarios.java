package com.ais.main;


import repos.PositionRep;
import repos.ProductRep;
import repos.WarehouseRep;

import java.util.ArrayList;
import java.util.Arrays;

public class Scenarios {

    public Scenarios(){}

    //прием товара
    public static boolean addProduct(Integer articul, String name, String description, Integer quantity, String warehouseName, String pos) {
        Position position = PositionRep.getForPosition(warehouseName, pos);
        if (position == null)
        {
            System.out.printf("ERROR: Позиция %s или склад %s не найдены \n", pos, warehouseName);// убрать сделать throw
            return false;
        }
        else{
            position.addProduct(new Product(articul, name, description, quantity, warehouseName, pos));
            ProductRep.save(new Product(articul, name, description, quantity, warehouseName, pos));
            System.out.printf("SUCCESS: Продукт с артикулом %s успешно добавлен \n", articul);
            return true;
        }
    }

    //создание позиции
    public static boolean createPosition(String position, String description, String warehouseName) throws Exception {
        //WarehouseRep warehouseRep = new WarehouseRep(Arrays.asList());
        Warehouse warehouse = WarehouseRep.getForName(warehouseName);
        Position positionS = PositionRep.getForPosition(warehouseName, position);
        if (warehouse != null)
            if(positionS == null && warehouse.addPositions(new Position(warehouseName, position, description, Arrays.asList())))
                {
                PositionRep.save(new Position(warehouseName, position, description, Arrays.asList()));
                System.out.printf("SUCCESS: Позиция %s на склад %s успешно добавлена \n", position, warehouseName);
                return true;
            }
            else
            {
                System.out.printf("ERROR: Позиция %s уже существует \n", position);
                return false;
            }
        else System.out.printf("ERROR: Cклад %s не найден \n", warehouseName);
        return false;
    }

    //выдача товара
    public static boolean writeOffProduct(Integer articul, Integer qnt){
        Product product = ProductRep.getForArticul(articul);
        if (product != null)
            if(product.writeOffProduct(qnt))
            {
                System.out.printf("SUCCESS: Товар с артикулом %d в количестве %d успешно выдан \n", articul, qnt);
                return true;
            }
            else System.out.printf("ERROR: Товар с артикулом %d не выдан (недостаточно количества на складе) \n", articul);
        return false;
    }

    public static void regWarehouse(String name, String address){
        WarehouseRep.save(new Warehouse(name, address, new ArrayList<>()));
    }
}
