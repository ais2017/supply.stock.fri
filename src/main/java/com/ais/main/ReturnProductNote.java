package com.ais.main;

import java.util.Date;
import java.util.Map;

/**
 * Created by admin on 24.11.2017.
 */
public class ReturnProductNote {
    private Map<Product, Integer> products;
    private Date date;
    private String workerName;
    private String clientName;
    private String status;

    public ReturnProductNote(Map<Product, Integer> products, Date date, String workerName, String clientName, String status) {
        this.products = products;
        this.date = date;
        this.workerName = workerName;
        this.clientName = clientName;
        this.status = status;
    }

    public Integer addProduct(Product product, Integer quantity) {
        return this.products.put(product, quantity);
    }

    public Map<Product, Integer> getProducts() {
        return products;
    }

    public Integer delProduct(Product product) {
        return this.products.remove(product);
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getWorkerName() {
        return workerName;
    }

    public void setWorkerName(String workerName) {
        this.workerName = workerName;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
