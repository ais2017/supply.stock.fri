package com.ais.main;

/**
 * Created by admin on 24.11.2017.
 */
public class Worker {
    private String workerName;
    private String type;

    public Worker(String workerName, String type) {
        this.workerName = workerName;
        this.type = type;
    }

    public String getWorkerName() {
        return workerName;
    }

    public void setWorkerName(String workerName) {
        this.workerName = workerName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
