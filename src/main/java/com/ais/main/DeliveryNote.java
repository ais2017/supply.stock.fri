package com.ais.main;

import java.util.Date;
import java.util.Map;

/**
 * Created by admin on 24.11.2017.
 */
public class DeliveryNote {
    private Map<Product, Integer> products;
    private Date date;
    private String workerName;
    private String provider;
    private String status;

    public DeliveryNote(Map<Product, Integer> products, Date date, String workerName, String provider, String status) {
        this.products = products;
        this.date = date;
        this.workerName = workerName;
        this.provider = provider;
        this.status = status;
    }

    public Integer addProduct(Product product, Integer quantity) {
        return this.products.put(product, quantity);
    }

    public Integer delProduct(Product product) {
        return this.products.remove(product);
    }

    public Map<Product, Integer> getProducts() {
        return products;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getWorkerName() {
        return workerName;
    }

    public void setWorkerName(String workerName) {
        this.workerName = workerName;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
