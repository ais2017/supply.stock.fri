package com.ais.main;

import java.util.Collection;

/**
 * Created by admin on 23.11.2017.
 */
public class Warehouse {
    private String name;
    private String address;
    private Collection<Position> positions;


    public Warehouse(String name, String address, Collection<Position> positions) {
        this.name = name;
        this.address = address;
        this.positions = positions;
    }

    public Collection<Position> getPositions() {
        return positions;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public boolean addPositions(Position position) {
        Position searchPosition = positions.stream().filter(iter ->
                iter.getPosition().equals(position.getPosition())).findFirst().orElse(null);
        if (searchPosition==null)
            return this.positions.add(position);
        return false;

    }

    public boolean delPositions(Position position) {
        return this.positions.remove(position);
    }

}
