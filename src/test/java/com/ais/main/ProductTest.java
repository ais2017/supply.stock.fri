package com.ais.main;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

/**
 * Created by admin on 24.11.2017.
 */
public class ProductTest {

    @Test
    public void test() {
        Product product = new Product(1,"name", "description", 5,"склад","позиция");

        Assert.assertTrue(product.getArticul().equals(1));
        Assert.assertTrue(product.getName().equals("name"));
        Assert.assertTrue(product.getDescription().equals("description"));
        Assert.assertTrue(product.getQuantity().equals(5));

        product.setName("new name");
        product.setDescription("new desc");
        product.setArticul(2);
        product.setQuantity(3);

        Assert.assertTrue(product.getArticul().equals(2));
        Assert.assertTrue(product.getName().equals("new name"));
        Assert.assertTrue(product.getDescription().equals("new desc"));
        Assert.assertTrue(product.getQuantity().equals(3));

    }

}
