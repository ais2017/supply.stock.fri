package com.ais.main;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

/**
 * Created by admin on 24.11.2017.
 */
public class WorkerTest {

    @Test
    public void test() {
        Worker worker = new Worker("workerName", "type");
        Assert.assertTrue(worker.getWorkerName().equals("workerName"));
        Assert.assertTrue(worker.getType().equals("type"));

        worker.setType("new type");
        worker.setWorkerName("new workerName");

        Assert.assertTrue(worker.getWorkerName().equals("new workerName"));
        Assert.assertTrue(worker.getType().equals("new type"));

    }

}
