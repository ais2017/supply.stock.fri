package com.ais.main;

import org.junit.Assert;
import org.junit.Test;

import java.util.Date;
import java.util.HashMap;

/**
 * Created by admin on 24.11.2017.
 */
public class ReturnProductNoteTest {

    @Test
    public void test() {
        Date dateForTest = new Date();
        Product productForTest = new Product(1,"name", "description", 5, "Склад", "позиция");

        ReturnProductNote returnProductNote= new ReturnProductNote(new HashMap<Product, Integer>(), dateForTest, "workerName", "clientName", "status");
        Assert.assertEquals(0, returnProductNote.getProducts().size());

        returnProductNote.addProduct(productForTest, 1);
        Assert.assertEquals(1, returnProductNote.getProducts().size());

        returnProductNote.setClientName("new clientName");
        returnProductNote.setDate(dateForTest);
        returnProductNote.setStatus("new status");
        returnProductNote.setWorkerName("new workerName");

        returnProductNote.delProduct(productForTest);
        Assert.assertEquals(0, returnProductNote.getProducts().size());

        Assert.assertTrue(returnProductNote.getDate().equals(dateForTest));
        Assert.assertTrue(returnProductNote.getClientName().equals("new clientName"));
        Assert.assertTrue(returnProductNote.getWorkerName().equals("new workerName"));
        Assert.assertTrue(returnProductNote.getStatus().equals("new status"));
    }

}
