package com.ais.main;

import org.junit.Assert;
import org.junit.Test;

import java.util.Date;
import java.util.HashMap;

/**
 * Created by admin on 24.11.2017.
 */
public class MoveProductNoteTest {

    @Test
    public void test() {
        Date dateForTest = new Date();
        Product productForTest = new Product(1,"name", "description", 5,"склад","позиция");

        MoveProductNote moveProductNote= new MoveProductNote(new HashMap<Product, Integer>(), dateForTest, "workerName", "warehouseProvider", "warehouseConsumer", "status");
        Assert.assertEquals(0, moveProductNote.getProducts().size());

        moveProductNote.addProduct(productForTest, 1);
        Assert.assertEquals(1, moveProductNote.getProducts().size());

        moveProductNote.setDate(dateForTest);
        moveProductNote.setStatus("new status");
        moveProductNote.setWarehouseConsumer("new warehouseConsumer");
        moveProductNote.setWarehouseProvider("new warehouseProvider");
        moveProductNote.setWorkerName("new workerName");

        moveProductNote.delProduct(productForTest);
        Assert.assertEquals(0, moveProductNote.getProducts().size());

        Assert.assertTrue(moveProductNote.getDate().equals(dateForTest));
        Assert.assertTrue(moveProductNote.getWarehouseProvider().equals("new warehouseProvider"));
        Assert.assertTrue(moveProductNote.getWarehouseConsumer().equals("new warehouseConsumer"));
        Assert.assertTrue(moveProductNote.getWorkerName().equals("new workerName"));
        Assert.assertTrue(moveProductNote.getStatus().equals("new status"));
    }

}
