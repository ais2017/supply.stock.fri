package com.ais.main;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by admin on 24.11.2017.
 */
public class PostitonTest {

    @Test
    public void test() {
        Product productForTest = new Product(1,"name", "description", 5,"склад","позиция");

        Position position = new Position("warehouseName","position", "description", new ArrayList<Product>());
        Assert.assertEquals(0, position.getProducts().size());

        position.addProduct(productForTest);
        Assert.assertEquals(1, position.getProducts().size());

        position.setDescription("new desc");
        position.setPosition("new position");

        Assert.assertTrue(position.getPosition().equals("new position"));
        Assert.assertTrue(position.getDescription().equals("new desc"));

        position.delProduct(productForTest);
        Assert.assertEquals(0, position.getProducts().size());
    }


}