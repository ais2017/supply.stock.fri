package com.ais.main;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

/**
 * Created by admin on 24.11.2017.
 */
public class WarehouseTest {

    @Test
    public void test() {
        Position posistionForTest = new Position("warehouseName", "position", "description", new ArrayList<Product>());
        Warehouse warehouse = new Warehouse("name", "address", new ArrayList<Position>());

        Assert.assertEquals(0, warehouse.getPositions().size());
        warehouse.addPositions(posistionForTest);

        Assert.assertFalse(warehouse.addPositions(posistionForTest));

        warehouse.setAddress("new address");
        warehouse.setName("new name");

        Assert.assertEquals(1, warehouse.getPositions().size());
        warehouse.delPositions(posistionForTest);
        Assert.assertEquals(0, warehouse.getPositions().size());

        Assert.assertTrue(warehouse.getName().equals("new name"));
        Assert.assertTrue(warehouse.getAddress().equals("new address"));
    }
}
