package com.ais.main;

import org.junit.*;
import repos.PositionRep;
import repos.ProductRep;
import repos.WarehouseRep;

import javax.validation.constraints.AssertFalse;
import javax.validation.constraints.AssertTrue;
import java.util.ArrayList;

import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

/**
 * Created by admin on 10.12.2017.
 */
public class ScenariosTest {

    @Before
    public void generateBD()
    {
        WarehouseRep.generateWarehouse();
        PositionRep.generatePosition();
        ProductRep.generateProducts();
    }

    @After
    public void cleanBD()
    {
        WarehouseRep.clear();
        PositionRep.clear();
        ProductRep.clear();

    }

    @Test
    public void addProductTest() {
        //повторяющийся артикул
        //Assert.assertFalse(Scenarios.addProduct(1, "name", "description", 5, "Склад №1", "A1"));

        //несуществующая позиция
        Assert.assertFalse(Scenarios.addProduct(10, "name", "description", 5, "Склад №1", "A5"));

        //несуществующий склад
        Assert.assertFalse(Scenarios.addProduct(10, "name", "description", 5, "Склад №11", "A5"));

        //успешный создание нового
        Assert.assertTrue(Scenarios.addProduct(10, "name", "description", 5, "Склад №1", "A1"));
        Assert.assertEquals("description", ProductRep.getForArticul(10).getDescription());
        Assert.assertEquals("name", ProductRep.getForArticul(10).getName());
        Assert.assertTrue(ProductRep.getForArticul(10).getQuantity().equals(5));
        //успешный добавление к старому
        Assert.assertTrue(Scenarios.addProduct(10, "name", "description", 5, "Склад №1", "A1"));
        Assert.assertTrue(ProductRep.getForArticul(10).getQuantity().equals(10));

    }


    @Test
    public void createPosition(){
        try {
            //несуществующий склад
            Assert.assertFalse(Scenarios.createPosition("position", "description", "warehouseName"));

            //существующая позиция
            Assert.assertFalse(Scenarios.createPosition("A1", "description", "Склад №2"));

            //успешный
            Assert.assertTrue(Scenarios.createPosition("A10", "description", "Склад №2"));
            Assert.assertEquals(PositionRep.getForPosition("Склад №2", "A10").getDescription(), "description");
        }
        catch (Exception e){

        }
    }

    @Test
    public void writeOffProduct(){
        //нет такого товара
        Assert.assertFalse(Scenarios.writeOffProduct(111,10));

        //недостаточно товара на складе
        Assert.assertFalse(Scenarios.writeOffProduct(1,10));

        //успешно
        Assert.assertTrue(Scenarios.writeOffProduct(1,1));
        Assert.assertTrue(ProductRep.getForArticul(1).getQuantity().equals(4));

    }

    @Test
    public void regWH(){
        //нет такого товара
        Scenarios.regWarehouse("whName", "whAddress");
        Assert.assertTrue(WarehouseRep.getForName("whName").getAddress().equals("whAddress"));
    }

    @Test(expected=IllegalArgumentException.class)
    public void checkRep() {

            Scenarios scenarios = new Scenarios();
            PositionRep positionRep = new PositionRep();
            ProductRep productRep = new ProductRep();
            WarehouseRep warehouseRep = new WarehouseRep();

            WarehouseRep.clear();
            PositionRep.clear();
            ProductRep.clear();
            Assert.assertTrue(WarehouseRep.getAll().isEmpty());
            Assert.assertTrue(PositionRep.getAll().isEmpty());
            Assert.assertTrue(ProductRep.getAll().isEmpty());
            Position position = new Position("whName", "pos", "desc", new ArrayList<Product>());
            PositionRep.save(position);
            PositionRep.save(position);

    }


}
