package com.ais.main;

import org.junit.Assert;
import org.junit.Test;

import java.util.Date;
import java.util.HashMap;

/**
 * Created by admin on 24.11.2017.
 */
public class DeliveryNoteTest {

    @Test
    public void test() {
        Date dateForTest = new Date();
        Product productForTest = new Product(1,"name", "description", 5,"склад","позиция");

        DeliveryNote deliveryNote = new DeliveryNote(new HashMap<Product, Integer>(), dateForTest, "workerName", "provider", "status");
        Assert.assertEquals(0, deliveryNote.getProducts().size());

        deliveryNote.addProduct(productForTest, 1);
        Assert.assertEquals(1, deliveryNote.getProducts().size());

        Date dateForTest2 = new Date();
        deliveryNote.setDate(dateForTest2);
        deliveryNote.setProvider("new provider");
        deliveryNote.setStatus("new status");
        deliveryNote.setWorkerName("new workerName");

        deliveryNote.delProduct(productForTest);
        Assert.assertEquals(0, deliveryNote.getProducts().size());

        Assert.assertTrue(deliveryNote.getDate().equals(dateForTest2));
        Assert.assertTrue(deliveryNote.getProvider().equals("new provider"));
        Assert.assertTrue(deliveryNote.getWorkerName().equals("new workerName"));
        Assert.assertTrue(deliveryNote.getStatus().equals("new status"));


    }
}
